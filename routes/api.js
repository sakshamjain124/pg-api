const express = require('express');
const router = express.Router();
const moongoose = require("mongoose");
const multer = require('multer');
// const checkAuth = require('../middleware/check-auth');

const storage = multer.diskStorage({
  destination:function(req, file, cb){
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null,  Date.now() + '-' + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' 
    || file.minetype === 'image/gif') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
const upload = multer({
    storage: storage, 
    limits:{
    fileSize: 1024 * 1024 *5 
},
 fileFilter: fileFilter

});  

const Pg = require('../models/pg');

//get a list of pg's from the db
router.get('/pg',(req, res, next) => { 
      //new
    Pg.find() 
    .exec()
    .then(docs => {
        // console.log(docs);
        //if (docs.length >=0){
            res.status(200).json(docs);
        //  } else{
        //        res.status(404).json({
        //            message: 'No etries found'
        //        });
        // }
    })
       .catch(err =>{
           console.log(err);
           res.status(500).json({
               error: err
           });
       });
    });
    

    //old
//     Pg.find(function (err, docs) {


//         if (err) {
//             res.json(err);
//         }
//         else {
//             res.json(docs);
//         }
//     });
// });

// get request by particular id 
 router.get('/pg/:id', (req, res, next) => {
    const id = req.params.pgId;
    Pg.findById(id)
      .exec()
      .then(docs => {
        // console.log("From database", doc);
        if (docs) {
          res.status(200).json(docs);
        } else {
          res
            .status(404)
            .json({ message: "No valid entry found for provided ID" });
        }
      })
      .catch(next);

     });
// post request add

 router.post('/pg', upload.single('pgImage'), (req, res, next) => {
    //new
    const pg = new Pg({
      state: req.body.state,
      city: req.body.city,
      address: req.body.address,
      name: req.body.name,
      email:req.body.email,
      phone:req.body.phone,
      price:req.body.price,
      pgImage:req.file.path  
    });
    pg
      .save()
      .then(result => {
        console.log(result);
        res.status(201).json({
          message: "Created  successfully",
          
      });
      })
      .catch(next);
  });
    //new finish



router.get('/pg', function (req, res, next) {
    Pg.find(function (err, docs) {
        if (err) {
            res.json(err);
        }
        else {
            res.json(docs);
        }
    });
});

// post request add
// router.post('/pg',  (req, res, next)=> {
//     console.log(req.body);
//     Pg.create(req.body).then(function (pg) {
//         res.status(200).json(
//             {
//                 pg,
//                 response: true
//             }
//         );
//     }).catch(next);
// });

//put request for changes  /:id
 

router.put('/pg/:id',  (req, res, next) => {
    Pg.findByIdAndUpdate({ _id: req.params.id }, req.body).then(function () {
        Pg.findOne({ _id: req.params.id }).then(function (pg) {
    
            res.send(pg);
            {  
                message: "DAta UPDATED"
            }         
        });
            res.send(pg);
        });


    });
    


// delete request for delete  /:id

// router.delete('/pg/:id', (req, res, next) => {
//     res.status(200).json({
//         message: "The Above Entry Got Deleted",
//         orderId: req.params.orderId

//     Pg.findByIdAndRemove({ _id: req.params.id }).then(function (pg) {
//         res.send(pg);
//     });
// });
//old code----delet requs by confirmng to delet
// router.delete('/pg/:id', function (req, res, next) {
//     Pg.findByIdAndRemove({ _id: req.params.id }).then(function (pg) {
//         res.send(pg);
//         res.status(200).json({
//                   message: "The Above Entry Got Deleted",
//                   orderId: req.params.orderId  });
//     });
       
// });


router.delete('/:pgId', (req, res, next) => {
  const id = req.params.pgId;
  Pg.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Product deleted',
          
      });
    })
    
});


module.exports = router;




