const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
// const routes = require('./routes/api/users');
const morgan = require("morgan");
const apiRoutes = require('./routes/api');
const userRoutes  = require('./routes/user');
const multer = require('multer');



// set up express app
const app = express();

//connect to mongodb
mongoose.connect('mongodb://localhost:27017/hostelwalle', {
// mongoose.connect('mongodb://localhost:27017/hostelwalle', {
// mongoose.connect('mongodb+srv://varun:hariom786@cluster0-r2jpw.mongodb.net/pgHostel?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then((res) => {
        console.log('Connection eastablished successfully');
    })

   .catch((err)=>{
        console.log(`Error while connecting to db ${err}`);
    });
    
// mongoose.Promise = global.Promise;
// parse application/x-www-form-urlencoded
//morgan
app.use(morgan('dev'));

app.use(bodyParser.urlencoded({extended: false }));
    


// mongoose.Promise = global.Promise;


//make upload folder public
app.use('/uploads',express.static('uploads'));
//huni kitta
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
      "Access-Control-Allow-Headers",
   "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
    return res.status(200).json({});   
    }
    next(); 
});



// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// // parse application/json
// app.use(bodyParser.json())
// app.use((req, res, next) => {
//     res.setHeader("Access-Control-Allow-Origin", "*");
//     res.setHeader("Access-Control-Allow-Credentials", true);
//     res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
//     res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, DELETE, OPTIONS");
//     next();
// });


// initialize routes
// app.use('/api', require('./routes/api'));

app.use('/api', apiRoutes);
app.use('/user', userRoutes);


//error handling midleware
app.use(function (err, req, res, next) {
    //    console.log(err);
    res.status(422).send({ error: err.message });
});




//listen for requests

app.listen(process.env.port || 3001, function () {
    console.log('now listening for requests');
});