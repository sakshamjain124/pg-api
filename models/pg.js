const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create PG Schema & Model
const PgSchema = new Schema({


      //  required: [ "state", "city", "address", "name","email","phone","price","photos"],

         state: {
            type: String,
            description: 'must be a string and is required',
            // required: true
         
         },
         city: {
            type: String,
            description: 'must be a string and is required',
            // required: true
         },
         address: {
            type: String,
            description: 'must be a string and is required',
            // required: true
         },
         name: {
            type: String,
            description: 'must be a string and is required',
            // required: true
         },
         email: {
            type: String,
            pattern : "@mongodb\.com$",
            description: 'must be a string and is required',
            // required: true
         },
         phone: {
            type: String,
            description: 'must be a string and is required',
            // required: true
         },
         price: {
            type: String,
            description: 'must be a string and is required',
            // required: true
         },
         pgImage: {
            type: String,
            description: 'must be a string and is required',
            required: true
         }
});
 module.exports = mongoose.model('pg', PgSchema);
